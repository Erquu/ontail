(function(global) {

	var Pane = function(container, parent) {
		this.domElement = document.createElement('div');
		this.parent = (parent || null);

		this._dragMenuItems = [];

		if (!!container) {
			container.appendChild(this.domElement);
		}

		this._splitPreview = document.createElement('div');
		this._splitPreview.className = 'splitpreview';
		this.domElement.appendChild(this._splitPreview);

		this._dragMenu = document.createElement('div');
		this._dragMenu.className = 'dragmenu';
		this._dragMenu.style.display = 'none';
		this.domElement.appendChild(this._dragMenu);

		var row = document.createElement('div');
		this._dragMenuUp = document.createElement('span');
		this._dragMenuUp.className = 'fa fa-arrow-up';
		this._dragMenuItems.push(this._dragMenuUp);
		row.appendChild(document.createElement('span'));
		row.appendChild(this._dragMenuUp);
		row.appendChild(document.createElement('span'));
		this._dragMenu.appendChild(row);

		row = document.createElement('div');
		this._dragMenuLeft = document.createElement('span');
		this._dragMenuLeft.className = 'fa fa-arrow-left';
		this._dragMenuItems.push(this._dragMenuLeft);
		row.appendChild(this._dragMenuLeft);

		this._dragMenuCenter = document.createElement('span');
		this._dragMenuCenter.className = 'fa fa-arrows';
		this._dragMenuItems.push(this._dragMenuCenter);
		row.appendChild(this._dragMenuCenter);

		this._dragMenuRight = document.createElement('span');
		this._dragMenuRight.className = 'fa fa-arrow-right';
		this._dragMenuItems.push(this._dragMenuRight);
		row.appendChild(this._dragMenuRight);

		this._dragMenu.appendChild(row);

		row = document.createElement('div');
		this._dragMenuDown = document.createElement('span');
		this._dragMenuDown.className = 'fa fa-arrow-down';
		this._dragMenuItems.push(this._dragMenuDown);
		row.appendChild(document.createElement('span'));
		row.appendChild(this._dragMenuDown);
		row.appendChild(document.createElement('span'));
		this._dragMenu.appendChild(row);

		this._bar = document.createElement('div');
		this._bar.className = 'bar';
		this.domElement.appendChild(this._bar);

		this._bl = document.createElement('ul');
		this._bar.appendChild(this._bl);

		this._add = document.createElement('li');
		this._add.innerHTML = '+';
		this._add.addEventListener('click', this);
		this._bl.appendChild(this._add);

		this._br = document.createElement('ul');
		this._bar.appendChild(this._br);

		this._content = document.createElement('div');
		this._content.className = 'content';
		this.domElement.appendChild(this._content);

		for (var i = 0; i < this._dragMenuItems.length; i++) {
			var item = this._dragMenuItems[i];
			item.addEventListener('dragenter', this, false);
			item.addEventListener('dragleave', this, false);
			item.addEventListener('dragover', this, false);
			item.addEventListener('drop', this, false);
		}

		this._activeTab = null;
		this._tabs = [];
	};

	Pane.prototype = {
		setActive: function(tab) {
			tryInvoke('deactivate', this._activeTab);
			this._activeTab = tab;
			tryInvoke('activate', this._activeTab);
			//this._updateSize();
		},
		addTab: function(tab, settings) {
			var result = false;
			var add = true;

			if (!!settings) {
				if (settings.split === 'horizontal' || settings.split === 'vertical') {
					if (this._tabs.length > 0) {
						var pane = new Pane();
						result = pane.addTab(tab);

						var left = this;
						var right = pane;

						if (settings.dir === 'right' || settings.dir === 'bottom') {
							left = pane;
							right = this;
						}

						if (!!this.parent) {
							fireEvent(this.parent, {
								type: 'panesplit',
								pane: this,
								left: left,
								right: right,
								horizontal: (settings.split === 'horizontal'),
								vertical: (settings.split === 'vertical')
							});
							add = false;
						}
					}
				}
			}

			// Prevent redoing all this when drag&drop withing the same pane
			if (add && tab !== this._activeTab) {
				this._bl.insertBefore(tab._tabElement, this._add);
				this._content.appendChild(tab._contentElement);

				tab._callback = this;

				tryInvoke('_tabRemoved', tab._pane, tab);
				tab._pane = this;

				this._tabs.push(tab);

				tryInvoke('deactivate', this._activeTab);
				this._activeTab = tab;
				tryInvoke('activate', this._activeTab);

				result = true;
			}
			return result;
		},
		showDragHandler: function(show) {
			this._dragMenu.style.display = show ? 'block' : 'none';
		},
		removeTab: function(tab) {
			var result = false;

			var index = this._tabs.indexOf(tab);
			if (index !== -1) {
				this._bl.removeChild(tab._tabElement);
				this._content.removeChild(tab._contentElement);
				
				if (this._tabs.length === 0) {
					if (!!this._parentContainer) {
						this._parentContainer.removePane(this);
					}
				} else {
					this._tabRemoved(tab);
				}

				result = true;
			}

			return result;
		},
		_tabRemoved: function(tab) {
			if (!!tab) {
				var index = this._tabs.indexOf(tab);
				if (index !== -1) {
					this._tabs.splice(index, 1);
					
					if (tab === this._activeTab) {
						if (this._tabs.length > 0) {
							index--;
							if (index > 0 && index < this._tabs.length) {
								this._activeTab = this._tabs[index];
							} else {
								this._activeTab = this._tabs[0];
							}
							tryInvoke('activate', this._activeTab);
						} else {
							this._activeTab = null;
						}
					}
				}
			}
			if (this._tabs.length === 0) {
				if (!!this.parent) {
					fireEvent(this.parent, {
						type: 'paneremove',
						pane: this
					});
				}
			}
		},
		resize: function(width, height) {
			this.domElement.style.width = width + 'px';

			var contentHeight = height - this._bar.offsetHeight;

			var style = window.getComputedStyle(this._content, null);
			if (!!style) {
				var paddingTop = pxToInt(style.getPropertyValue("padding-top"));
				var paddingBottom = pxToInt(style.getPropertyValue("padding-bottom"));

				if (paddingTop !== null) {
					contentHeight -= paddingTop;
				}
				if (paddingBottom !== null) {
					contentHeight -= paddingBottom;
				}
			}

			this._content.style.height = contentHeight + 'px';

			this._dragMenu.style.top = (this._content.offsetTop + this._content.offsetHeight * 0.5) + 'px';
			this._dragMenu.style.left = (this._content.offsetLeft + this._content.offsetWidth * 0.5) + 'px';

			tryInvoke('resize', this._activeTab, width, contentHeight);
		},
		_dragHandler: function(ev) {
			var target = ev.target || ev.srcElement;
			switch (target) {
				case this._dragMenuUp:
					this._splitPreview.style.top = this._content.offsetTop + 'px';
					this._splitPreview.style.left = this._content.offsetLeft + 'px';
					this._splitPreview.style.width = this._content.offsetWidth + 'px';
					this._splitPreview.style.height = (this._content.offsetHeight * 0.5) + 'px';
					break;
				case this._dragMenuDown:
					var halfHeight = (this._content.offsetHeight * 0.5);
					this._splitPreview.style.top = (this._content.offsetTop + halfHeight) + 'px';
					this._splitPreview.style.left = this._content.offsetLeft + 'px';
					this._splitPreview.style.width = this._content.offsetWidth + 'px';
					this._splitPreview.style.height = halfHeight + 'px';
					break;
				case this._dragMenuLeft:
					this._splitPreview.style.top = this._content.offsetTop + 'px';
					this._splitPreview.style.left = this._content.offsetLeft + 'px';
					this._splitPreview.style.width = (this._content.offsetWidth * 0.5) + 'px';
					this._splitPreview.style.height = this._content.offsetHeight + 'px';
					break;
				case this._dragMenuRight:
					var halfWidth = (this._content.offsetWidth * 0.5);
					this._splitPreview.style.top = this._content.offsetTop + 'px';
					this._splitPreview.style.left = (this._content.offsetLeft + halfWidth) + 'px';
					this._splitPreview.style.width = halfWidth + 'px';
					this._splitPreview.style.height = this._content.offsetHeight + 'px';
					break;
				case this._dragMenuCenter:
					this._splitPreview.style.top = this._content.offsetTop + 'px';
					this._splitPreview.style.left = this._content.offsetLeft + 'px';
					this._splitPreview.style.width = this._content.offsetWidth + 'px';
					this._splitPreview.style.height = this._content.offsetHeight + 'px';
					break;
			}
		},
		_dropHandler: function(ev) {
			ev.stopPropagation();
			ev.preventDefault();

			ui.showDragHandler(false);

			var target = ev.target || ev.srcElement;
			var hideSplit = false;
			switch (target) {
				case this._dragMenuUp:
					hideSplit = true;
					this.addTab(ui.dragElement, {
						split: 'vertical',
						dir: 'top'
					});
					break;
				case this._dragMenuDown:
					hideSplit = true;
					this.addTab(ui.dragElement, {
						split: 'vertical',
						dir: 'bottom'
					});
					break;
				case this._dragMenuLeft:
					hideSplit = true;
					this.addTab(ui.dragElement, {
						split: 'horizontal',
						dir: 'right'
					});
					break;
				case this._dragMenuRight:
					hideSplit = true;
					this.addTab(ui.dragElement, {
						split: 'horizontal',
						dir: 'left'
					});
					break;
				case this._dragMenuCenter:
					hideSplit = true;
					this.addTab(ui.dragElement);
					break;
			}
			if (hideSplit) {
				this._splitPreview.style.display = 'none';
			}
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case 'click':
					var target = ev.target || ev.srcElement;
					if (target === this._add) {
						ui.createPage('logselector', this);
					}
					break;
				case 'pageselect':
					this.setActive(ev.page);
					break;
				case 'pagedelete':
					this.removeTab(ev.page);
					tryInvoke('dispose', ev.page);
					break;
				case 'dragenter':
					this._splitPreview.style.display = 'block';
					break;
				case 'dragleave':
					this._splitPreview.style.display = 'none';
					break;
				case 'dragover':
	  				ev.preventDefault();
	  				this._dragHandler(ev);
	  				break;
	  			case 'drop':
	  				this._dropHandler(ev);
	  				break;
  			}
		}
	};

	global.Pane = Pane;

})(this);
(function(global) {

	var SplitContainer = function(left, right) {
		this._splitDirectionHorizontal = true; // left | right
		this._leftWidth = 0.5; // or top

		this._left = left;
		this._right = right;

		this._left.parent = this;
		this._right.parent = this;

		this._currentWidth = 0;
		this._currentHeight = 0;

		this.domElement = document.createElement('div');

		if (!!this._left.domElement.parentNode) {
			this._left.domElement.parentNode.removeChild(this._left.domElement);
		}
		this.domElement.appendChild(this._left.domElement);

		this._splitter = document.createElement('div');
		this._splitter.className = 'splitter';
		this._splitter.draggable = true;
		this._splitter.addEventListener('dragstart', this);
		this.domElement.appendChild(this._splitter);

		if (!!this._right.domElement.parentNode) {
			this._right.domElement.parentNode.removeChild(this._right.domElement);
		}
		this.domElement.appendChild(this._right.domElement);
	};

	SplitContainer.prototype = {
		showDragHandler: function(show) {
			tryInvoke('showDragHandler', this._left, show);
			tryInvoke('showDragHandler', this._right, show);
		},
		addTab: function(tab, settings) {
			if (!!this._left && this._left.addTab(tab, settings)) {
				return true;
			}
			if (!!this._right && this._right.addTab(tab, settings)) {
				return true;
			}
			return false;
		},
		removePane: function(pane) {
			var remove = (pane === this._left || pane === this._right);

			if (remove) {
				fireEvent(this.parent, {
					type: 'containerremove',
					container: this,
					pane: (pane === this._left ? this._right : this._left)
				});
			}
		},
		resize: function(width, height) {
			this._currentWidth = width;
			this._currentHeight = height;

			if (this._splitDirectionHorizontal) {
				var splitterWidth = this._splitter.offsetWidth;
				var splitterHalfWidth = splitterWidth * 0.5;

				var leftWidth = width * this._leftWidth - splitterHalfWidth;
				var rightWidth = width * (1.0 - this._leftWidth) - splitterHalfWidth;

				this._left.resize(leftWidth, height);
				this._right.resize(rightWidth, height);

				this._splitter.style.width = null;
				this._splitter.style.height = height + 'px';

				this.domElement.className = 'split split-horizontal';
			} else {
				var splitterHeight = this._splitter.offsetHeight;
				var splitterHalfHeight = splitterHeight * 0.5;

				var topHeight = height * this._leftWidth - splitterHalfHeight;
				var bottomHeight = height * (1.0 - this._leftWidth) - splitterHalfHeight;

				this._left.resize(width, topHeight);
				this._right.resize(width, bottomHeight);

				this._splitter.style.width = width + 'px';
				this._splitter.style.height = null;

				this.domElement.className = 'split split-vertical';
			}
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case 'paneremove':
					this.removePane(ev.pane);
					break;
				case 'containerremove':
					this.domElement.insertBefore(ev.pane.domElement, ev.container.domElement);
					this.domElement.removeChild(ev.container.domElement);
					
					if (ev.container === this._left) {
						this._left = ev.pane;
					} else if (ev.container === this._right) {
						this._right = ev.pane;
					}

					ev.pane.parent = this;
					
					ui._updateSize();
					break;
				case 'panesplit':
					var sc = new SplitContainer(ev.left, ev.right);
					sc.parent = this;

					if (ev.horizontal) {
						sc._splitDirectionHorizontal = true;
					} else {
						sc._splitDirectionHorizontal = false;
					}

					if (ev.pane === this._left) {
						this._left = sc;
						this.domElement.insertBefore(this._left.domElement, this._splitter);
					} else {
						this._right = sc;
						this.domElement.appendChild(this._right.domElement);
					}

					ui._updateSize();
					break;
				case 'dragstart':
					var axis = this._splitDirectionHorizontal ? 'clientX' : 'clientY';
					var pos = ev[axis];

					this.startPos = pos;
					
					this.domElement.parentNode.addEventListener('mousemove', this);
					window.addEventListener('mouseup', this);

					ev.preventDefault();
					ev.stopPropagation();
					return false;
				case 'mouseup':
					window.removeEventListener('mouseup', this);
					this.domElement.parentNode.removeEventListener('mousemove', this);
					break;
				case 'mousemove':
					var axis = this._splitDirectionHorizontal ? 'clientX' : 'clientY';
					var percDir = this._splitDirectionHorizontal ? '_currentWidth' : '_currentHeight';

					var pos = ev[axis];
					var movement = pos - this.startPos;
					// TODO: ohh not happy with the relative offset...
					// try to calc the mouse pos for the container instead
					var diff = 1 / this[percDir] * movement;

					var nw = this._leftWidth + diff;
					this._leftWidth = Math.min(Math.max(nw, 0.1), 0.9);
					this.resize(this._currentWidth, this._currentHeight);

					this.startPos = pos;
					break;
			}
		}
	};

	global.SplitContainer = SplitContainer;

})(this);
/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var TabPage = (function() {
	var TabPage = function(callback) {
		//
		// SETTINGS
		//
		this._closeable = true;
		this._active = false;

		//
		// FIELDS
		//
		this._callback = callback;
		this._title = 'Unnamed tab';
		this._pane = null;

		//
		// ELEMENTS
		//
		this._tabElement = document.createElement('li');
		this._tabElement.addEventListener('mousedown', this);

		this._tabTitle = document.createElement('div');
		this._tabElement.appendChild(this._tabTitle);

		this._tabClose = document.createElement('div');

		this._close = document.createElement('i');
		this._close.className = 'fa fa-close';
		this._tabClose.appendChild(this._close);

		this._tabElement.appendChild(this._tabClose);

		this._contentElement = document.createElement('div');
		this._contentElement.id = 'content';

		this._contentPage = undefined;

		Object.defineProperty(this, 'title', {
			get: function() {
				return this._title;
			},
			set: function(title) {
				var oldTitle = this._title;

				this._title = title;
				this._tabTitle.innerHTML = title;
				fireEvent(this._callback, {
					type: 'pagerename',
					page: this,
					newTitle: title,
					oldTitle: oldTitle
				});
			}
		});
	};

	TabPage.prototype = {
		activate: function() {
			// Called when the tab is selected
			this._active = true;
			this._tabElement.className = 'active';
			this._contentElement.style.display = 'block';

			tryInvoke('activate', this._contentPage);
		},
		deactivate: function() {
			// Called when the tab is deactivated/unselected
			this._active = false;
			this._tabElement.className = '';
			this._contentElement.style.display = 'none';

			tryInvoke('deactivate', this._contentPage);
		},
		resize: function(width, height) {
			tryInvoke('resize', this._contentPage, width, height);
		},
		selfDestroy: function() {
			fireEvent(this._callback, {
				type: 'pagedelete',
				page: this
			});
		},
		dispose: function() {
			tryInvoke('dispose', this._contentPage);
		},
		poll: function() {
			tryInvoke('poll', this._contentPage);
		},
		getTabElement: function() {
			return this._tabElement;
		},
		getContentElement: function() {
			return this._contentElement;
		},
		getContentPage: function() {
			return this._contentPage;
		},
		setContentPage: function(page) {
			this._contentPage = page;
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case 'mousedown':
					ev.srcElement = ev.srcElement || ev.target;

					if (ev.button === 1 || (ev.srcElement === this._tabClose || ev.srcElement == this._close)) {
						// Middle Mouse Click
						fireEvent(this._callback, {
							type: 'pagedelete',
							page: this
						});
					} else {
						// Left Mouse Click
						fireEvent(this._callback, {
							type: 'pageselect',
							page: this
						});
					}
					break;
			}
		}
	};

	return TabPage;
})();
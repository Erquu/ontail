var CssMod = (function() {

	var head = null;

	var getHead = function() {
		if (head === null)
			head = document.getElementsByTagName("head")[0];
		return head;
	};

	var CssMod = function() {
		this._container = null;
		this._active = false;
	};

	CssMod.prototype = {
		_getStyleNode: function() {
			if (this._container === null) {
				this._container = document.createElement("style");
				this._container.type = "text/css";
			}
			if (!this._active) {
				getHead().appendChild(this._container);
				this._active = true;
			}
			return this._container;
		},
		clear: function() {
			this._getStyleNode().innerHTML = "";
			if (this._active) {
				getHead().removeChild(this._getStyleNode());
				this._active = false;
			}
		},
		add: function(css) {
			this._getStyleNode().innerHTML += css + "\n";
		},
		addStyle: function(selector, style) {
			this._getStyleNode().innerHTML += selector + "{" + style + "}\n";
		},
		createNewMod: function() {
			return new CssMod();
		}
	};

	return new CssMod();
})();
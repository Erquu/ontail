/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var StyleChanger = (function() {
	var StyleChanger = function() {
		this._styles = [];
		this._activeStyle = null;

		window.addEventListener("load", this);
	};

	StyleChanger.prototype = {
		getStyleNames: function() {
			var names = [];
			for (var i = 0; i < this._styles.length; i++) {
				names.push(this._styles[i].name);
			}
			return names;
		},
		setActiveStyle: function(name) {
			if (!!this._styles[name]) {
				var style = this._styles[name];

				if (!!this._activeStyle) {
					this._activeStyle.disabled = true;
				}
				this._activeStyle = style.element;
				this._activeStyle.disabled = false;

				setCookie("style", style.name, 30);
			}
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case "load": 
					var i;
					var link;

					var links = document.getElementsByTagName("head")[0].getElementsByTagName("link");
					for (i = 0; i < links.length; i++) {
						link = links[i];
						if (link.rel.toLowerCase().indexOf("stylesheet") > -1) {
							var style = {
								name: link.title,
								element: link
							};
							this._styles[link.title] = style;
							this._styles.push(style);
						}
					}

					var active = getCookie("style");
					if (!!active) {
						for (i = 0; i < this._styles.length; i++) {
							link = this._styles[i];
							if (link.name === active) {
								this._activeStyle = link.name;
								link.element.disabled = false;
							} else {
								link.element.disabled = true;
							}
						}
					}
					break;
			}
		}
	};

	return new StyleChanger();
})();
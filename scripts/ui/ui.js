/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var ui = (function() {

	var gotoParentNode = function(element, tagName) {
		if (!!element) {
			if (element.tagName.toLowerCase() == tagName.toLowerCase()) {
				return element;
			} else {
				return gotoParentNode(element.parentNode, tagName);
			}
		}
		return null;
	};

	var Ui = function() {
		this._config = null;

		this._pageOpenHandler = {};
		this._tabPages = [];
		this._openPages = {};
		this._renderers = [];

		this._pane = null;
		this._defaultPage = null;
		this._startPage = null;
	};

	Ui.prototype = {
		_loadRendererFromConfig: function() {
			if (!!this._config.renderer) {
				for (var i = 0; i < this._config.renderer.length; i++) {
					var renderer = new Renderer(this._config.renderer[i]);
					this.registerRenderer(renderer);
				}
			}
		},
		_loadRendererFromStorage: function() {
			if (Storage.isSupported()) {
				var rules = Storage.getJSON("rules");
				if (rules !== null) {
					for (var i = 0; i < rules.length; i++) {
						var renderer = new Renderer(rules[i]);
						this.registerRenderer(renderer);	
					}
				}
			}
		},
		init: function() {
			this._pane = new Pane(document.body, this);

			/*
			this._fullscreenButton = document.getElementById("fullscreen");
			this._fullscreenIcon = this._fullscreenButton.firstElementChild;
			if (fullscreenEnabled()) {
				this._fullscreenButton.style.display = 'block';
				this._fullscreenButton.addEventListener("click", this, true);

				if ('onfullscreenchange' in document)
					document.addEventListener("fullscreenchange", this);
				else if ('onmozfullscreenchange' in document)
					document.addEventListener("mozfullscreenchange", this);
				else if ('onwebkitfullscreenchange' in document)
					document.addEventListener("webkitfullscreenchange", this);
			}

			this._configButton = document.getElementById("config");
			this._configButton.addEventListener("click", this, true);

			this._infoButton = document.getElementById("info");
			this._infoButton.addEventListener("click", this, true);
			*/

			this._updateSize();

			window.addEventListener("resize", this);
			window.addEventListener("beforeunload", this);

			window.addEventListener("keydown", this);
		},
		/**
		 * 
		 */
		updateConfig: function(config) {
			this._config = config || {};

			if (!!this._config.title) {
				document.title = this._config.title;
			}

			if (!!this._config.startPage) {
				this._startPage = this._config.startPage;
			}

			if (!!this._config.defaultPage) {
				this._defaultPage = this._config.defaultPage;
			}

			if (!!this._config.pages) {

				var pageFolder = this._config.pageFolder || "";
				if (pageFolder.length > 1 && pageFolder.substr(-1) !== '/') {
					pageFolder += '/';
				}

				for (var i = 0; i < this._config.pages.length; i++) {
					var name = this._config.pages[i];
					if (name.length > 3 && name.toLowerCase().substr(-3) !== '.js') {
						name += '.js';
					}
					var path = pageFolder + name;
					var tag = document.createElement('script');
					tag.src = path;
					document.head.appendChild(tag);
				}
			}

			this.resetRenderer();
		},
		registerPageOpenHandler: function(name, callback) {
			this._pageOpenHandler[name] = callback;

			if (name === this._startPage) {
				this.createPage(name);
			}
		},
		createPage: function(name, pane) {
			if (name in this._pageOpenHandler) {
				var page = this._pageOpenHandler[name](this);
				this.addTab(page, pane);
				return true;
			}
			return false;
		},
		hasPageWithTitle: function(title) {
			if (title in this._openPages) {
				return this._openPages[title] === true;
			}
			return false;
		},
		getPagesByTitle: function(title) {
			var resultPages = [];
			for (var i = 0; i < this._tabPages.length; i++) {
				if (this._tabPages[i].title === title) {
					resultPages.push(this._tabPages[i]);
				}
			}
			return resultPages;
		},
		/**
		 * Reloads all renderers
		 */
		resetRenderer: function() {
			this._renderers = [];

			this._loadRendererFromConfig();
			this._loadRendererFromStorage();
		},
		/**
		 * Registers a new renderer for incoming lines
		 * @param renderer {Renderer} The renderer to be registered
		 */
		registerRenderer: function(renderer) {
			if (this._renderers.indexOf(renderer) === -1) {
				this._renderers.push(renderer);
			}
		},
		/**
		 * Lets the registered renderers render the given line
		 * @param line {string} The line to be rendered
		 * @param meta {object} The meta information created by the renderer
		 *                      This will be used as a reference to give
		 *                      additional information to the caller 
		 * @return {string} The rendered line
		 */
		renderLine: function(line, meta) {
			meta = meta || {};
			meta.success = false;
			meta.wholeLine = false;
			meta.renderer = [];

			for (var i = 0; i < this._renderers.length; i++) {
				var renderer = this._renderers[i];

				var result = renderer.apply(line);
				if (result !== false) {
					meta.success = true;
					meta.renderer.push(renderer);

					if (renderer._wholeLine || renderer.isBlocking()) {
						meta.wholeLine = renderer._wholeLine;
						return result;
					} else {
						// If all renderers should apply their rules,
						// concatenate all results and return after the loop
						line = result;
					}
				}
			}
			return line;
		},
		/**
		 * Interval callback to poll for file changes (tail)
		 */
		poll: function() {
			for (var i = 0; i < this._tabPages.length; i++) {
				tryInvoke('poll', this._tabPages[i]);
			}
		},
		createTabPage: function() {
			var tabPage = new TabPage(this);

			tabPage._tabElement.draggable = true;

			var fun = (function(self, tabPage) {
				return function(ev) {
					ev.tabPage = tabPage;
					self.handleEvent(ev);
				};
			})(this, tabPage);

			tabPage._tabElement.addEventListener('dragstart', fun, false);
			tabPage._tabElement.addEventListener('dragstop', this, false);
			tabPage._tabElement.addEventListener('dragover', this, false);
			tabPage._tabElement.addEventListener('dragleave', this, false);
			tabPage._tabElement.addEventListener('drop', fun, false);

			return tabPage;
		},
		addTab: function(contentPage, pane, active) {
			active = (this._activePage === null || active !== false);

			var tabPage = this.createTabPage();
			tryInvoke('init', contentPage, tabPage, this);
			tabPage.setContentPage(contentPage);

			if (!pane) {
				pane = this._pane;
			}
			pane.addTab(tabPage);

			this._tabPages.push(tabPage);
			this._openPages[tabPage.title] = true;
		},
		/**
		 * Function to update the size of the currently active TabPage
		 */
		_updateSize: function() {
			var w = window.innerWidth;
			var h = window.innerHeight;
			tryInvoke('resize', this._pane, w, h);
		},
		showDragHandler: function(show) {
			tryInvoke('showDragHandler', this._pane, show);
			this._updateSize();
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case "resize":
					this._updateSize();
					break;
				case "beforeunload":
					for (var i = 0; i < this._tabPages.length; i++) {
						tryInvoke('dispose', this._tabPages[i]);
					}
					break;
				case "fullscreenchange":
				case "mozfullscreenchange":
				case "webkitfullscreenchange":
					if (isFullScreen()) {
						this._fullscreenIcon.className = 'fa fa-compress';
					} else {
						this._fullscreenIcon.className = 'fa fa-expand';
					}
					break;
				case "keydown":
					var target = ev.target;
					if (!!target && target.tagName !== 'INPUT') {
						switch (ev.keyCode) {
							case 39:
								
								break;
						}
					}
					break;
				case "click":
					ev.srcElement = ev.srcElement || ev.target;

					if (isParent(ev.srcElement, this._addButton)) {
						this.createPage('logselector');
					} else if (isParent(ev.srcElement, this._configButton)) {
						this.createPage('config');
					} else if (isParent(ev.srcElement, this._fullscreenButton)) {
						if (isFullScreen()) {
							exitFullscreen();
						} else {
							enterFullscreen();
						}
					} else if (isParent(ev.srcElement, this._infoButton)) {
						this.createPage('info');
					}
					break;
				case "pageselect":
					this.setActive(ev.page);
					break;
				case "pagedelete":
					this.remove(ev.page);
					break;
				case "pagerename":
					if (ev.oldTitle in this._openPages) {
						this._openPages[ev.oldTitle] = null;
					}
					this._openPages[ev.newTitle] = true;
					break;
				case 'dragstart':
					this.dragElement = ev.tabPage;
					// Firefox needs data to drag around
    				ev.dataTransfer.setData('Text', 'drag');

					this.showDragHandler(true);
					break;
				case 'dragstop':
					this.dragElement = null;
					this.showDragHandler(false);

					ev.stopPropagation();
					ev.preventDefault();
					break;
				case 'dragover':
	  				ev.preventDefault();

					if (!!this.dragElement) {
						var source = this.dragElement._tabElement;
						var target = gotoParentNode(ev.toElement || ev.target, 'li');

						var clientX = ev.layerX;
						var hw = target.offsetLeft + (target.offsetWidth * 0.5);

						if (target != source) {
							if (clientX < hw) {
								target.classList.add('dragLeft');
								target.classList.remove('dragRight');
							} else {
								target.classList.remove('dragLeft');
								target.classList.add('dragRight');
							}
						}
	  				}
					break;
				case 'dragleave':
					if (!!this.dragElement) {
						var source = this.dragElement;
						var target = gotoParentNode(ev.toElement || ev.target, 'li');

						if (target != source) {
							target.classList.remove('dragLeft');
							target.classList.remove('dragRight');
						}
					}
					break;
				case 'drop':
					ev.stopPropagation();
					ev.preventDefault();

					this.showDragHandler(false);

					if (!!this.dragElement) {
						var source = this.dragElement;
						var target = ev.tabPage;

						var clientX = ev.layerX;
						var hw = target._tabElement.offsetLeft + (target._tabElement.offsetWidth * 0.5);

						if (target != source) {
							ev.tabPage._pane.addTab(this.dragElement);

							if (clientX < hw) {
								source._tabElement.parentNode.insertBefore(
									source._tabElement,
									target._tabElement);
							} else {
								// There is always the [+] button, so nextSibling can not be undefined at this point
								source._tabElement.parentNode.insertBefore(
									source._tabElement,
									target._tabElement.nextSibling);
							}

							target._tabElement.classList.remove('dragLeft');
							target._tabElement.classList.remove('dragRight');
						}
					}
					break;
				case 'paneremove':
					break;
				case 'containerremove':
					if (ev.container === this._pane) {
						document.body.removeChild(this._pane.domElement);
						this._pane = ev.pane;
						this._pane.parent = this;
						document.body.appendChild(this._pane.domElement);

						this._updateSize();
					}
					break;
				case 'panesplit':
					var sc = new SplitContainer(ev.left, ev.right);
					sc.parent = this;

					if (ev.horizontal) {
						sc._splitDirectionHorizontal = true;
					} else {
						sc._splitDirectionHorizontal = false;
					}

					this._pane = sc;

					document.body.appendChild(sc.domElement);

					this._updateSize();
					break;
			}
		}
	};

	return new Ui();
})();
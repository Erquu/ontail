/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

(function(global) {
	var Renderer = function(patternOrSettings) {
		this._styles = [];
		this._styleString = "";
		this._word = null;
		this._wholeLine = false;
		this._blocking = false;

		if (typeof patternOrSettings === "string") {
			this._word = patternOrSettings;
		} else {
			if (patternOrSettings.pattern !== undefined) { this._word = patternOrSettings.pattern; }
			if (patternOrSettings.line !== undefined) { this._wholeLine = patternOrSettings.line; }
			if (patternOrSettings.blocking !== undefined) { this._blocking = patternOrSettings.blocking; }
			if (patternOrSettings.styles !== undefined) { this.addStyles(patternOrSettings.styles); }

		}
	};

	Renderer.prototype = {
		_applyStyleOnElement: function(e) {
			e.setAttribute("style", this._styleString);
		},
		_getCss: function(name) {
			for (var i = 0; i < this._styles.length; i++) {
				if (this._styles[i].name === name) {
					return this._styles[i];
				}
			}
			return undefined;
		},
		_renderStyleString: function() {
			var tmp = "";
			for (var i = 0; i < this._styles.length; i++) {
				var style = this._styles[i];
				tmp += style.name + ":" + style.value + ";";
			}
			this._styleString = tmp;
		},
		_addCssValue: function(name, value) {
			var style = this._getCss(name);
			if (style === undefined) {
				style = {};
				style.name = name;
				style.value = value;
				this._styles.push(style);
			} else {
				style.value = value;
			}
			return style;
		},
		isBlocking: function() {
			return this._blocking;
		},
		addStyles: function(styles) {
			for (var i = 0; i < styles.length; i++) {
				this._addCssValue(styles[i].name, styles[i].value);
			}
			this._renderStyleString();
		},
		apply: function(line) {
			var pattern = new RegExp(this._word, "gi");
			if (pattern.test(line)) {
				if (this._wholeLine) {
					var e = document.createElement("div");
					this._applyStyleOnElement(e);
					e.appendChild(document.createTextNode(line));
					return e.outerHTML;
				} else {
					var e = document.createElement("span");
					this._applyStyleOnElement(e);
					line = line.replace(new RegExp(this._word, "gi"), function(match, offset, string) {
						e.appendChild(document.createTextNode(match));
						return e.outerHTML;
					});
					return line;
				}
			}
			return false;
		}
	};

	var WordRenderer = function(word, color, bold, additionalStyles) {
		Renderer.call(this, word);

		this._styles["color"] = color;
		this._styles["fontWeight"] = bold || false;

		if (!!additionalStyles) {
			this.addStyles(additionalStyles);
		}
	};
	WordRenderer.prototype = Object.create(Renderer.prototype);
	WordRenderer.prototype.constructor = WordRenderer;

	var LineRenderer = function(word, background, color, bold, additionalStyles) {
		Renderer.call(this, word);

		this._wholeLine = true;

		this._styles["background"] = background;
		this._styles["color"] = color;
		this._styles["fontWeight"] = bold || false;

		if (!!additionalStyles) {
			this.addStyles(additionalStyles);
		}
	};
	LineRenderer.prototype = Object.create(Renderer.prototype);
	LineRenderer.prototype.constructor = LineRenderer;

	global.Renderer = Renderer;
	global.WordRenderer = WordRenderer;
	global.LineRenderer = LineRenderer;
})(this);
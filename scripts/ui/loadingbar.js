/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var LoadingBar = (function() {
	var LoadingBar = function() {
		this.domElement = null;
		this._indicator = null;
		this._visible = false;
		this._currentValue = 0;
		this._maxValue = 0;
	};

	LoadingBar.prototype = {
		_createDom: function() {
			this.domElement = document.createElement("div");
			this.domElement.className = "loading";
			this._indicator = document.createElement("div");
			this.domElement.appendChild(this._indicator);
		},
		show: function(container, maxValue) {
			if (!this._visible) {
				this._visible = true;
				
				this._currentValue = 0;
				this._maxValue = maxValue;

				if (!this.domElement) {
					this._createDom();
				}
				document.body.appendChild(this.domElement);
				this.domElement.style.display = "block";

				var w = this.domElement.offsetWidth;
				var cw = container.offsetWidth;
				this.domElement.style.left = (cw * 0.5 - w * 0.5) + "px";

				var h = this.domElement.offsetHeight;
				var ch = container.offsetHeight;
				this.domElement.style.top = (container.offsetTop + (ch * 0.5 - h * 0.5)) + "px";

				this._indicator.style.width = "0px";
			}
		},
		update: function(value) {
			var totalWidth = this.domElement.offsetWidth;
			var pWidth = totalWidth / this._maxValue;
			var width = Math.round(value * pWidth);
			this._indicator.stlye.width = width + "px";
		},
		hide: function() {
			document.body.removeChild(this.domElement);
			this._visible = false;
		}
	};

	return LoadingBar;
})();
/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

function setCookie(name, value, lifespanDays, validDomain) {
	var domainString = validDomain ? ("; domain=" + validDomain) : '' ;
	document.cookie =	name + "=" + encodeURIComponent(value) +
						"; max-age=" + 60 * 60 * 24 * lifespanDays +
						"; path=/" + domainString ;
}

function getCookie(name) {
	var cookie_string = document.cookie ;
    if (cookie_string.length !== 0) {
        var cookie_value = cookie_string.match('(^|;?)\\s*' + name + '=([^;]*)');
        if (cookie_value !== null)
        	return decodeURIComponent(cookie_value[2]) ;
    }
    return null;
}

function htmlspecialchars(line) {
    var e = document.createElement("div");
    e.appendChild(document.createTextNode(line));
    
    return e.innerHTML;
}

function tryInvoke(fName, instance) {
  if (typeof fName === 'function') {
    fName.apply(this, Array.prototype.slice.call(arguments, 1));
  } else if (!!instance && typeof instance[fName] === 'function') {
    instance[fName].apply(instance, Array.prototype.slice.call(arguments, 2));
  }
}

function guid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = crypto.getRandomValues(new Uint8Array(1))[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
  });
}

function enterFullscreen() {
  var elem = document.documentElement;

  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) {
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) {
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) {
    elem.msRequestFullscreen();
  }
}

function exitFullscreen() {
  var elem = document.documentElement;

  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen();
  }
}

function fullscreenEnabled() {
  if ('fullscreenEnabled' in document) {
    return document.fullscreenEnabled;
  } else if ('mozFullScreenEnabled' in document) {
    return document.mozFullScreenEnabled;
  } else if ('webkitFullscreenEnabled' in document) {
    return document.webkitFullscreenEnabled;
  }
  return false;
}

function isFullScreen() {
  /*if ('fullscreenEnabled' in document) {
    return document.fullscreenEnabled;
  } else */if ('mozFullScreen' in document) {
    return document.mozFullScreen;
  } else if ('webkitIsFullScreen' in document) {
    return document.webkitIsFullScreen;
  }
  return false;
}

function isParent(element, parent) {
  do {
    if (element === parent) {
      return true;
    }
  } while (!!(element = element.parentNode));
  return false;
}
/**
 * Parses a css pixel (e.g. "200px") string to extract the number
 * in a number type
 * @param str {string} The string to be parsed
 * @return {number} The parsed number or null if the string could
 *                  not be parsed
 */
var pxToInt = function(str) {
  if (!!str && str.length > 2) {
    var num = parseInt(str.substring(0, str.length - 2));
    if (!isNaN(num)) {
      return num;
    }
  }
  return null;
};
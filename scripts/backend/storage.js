/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var Storage = (function(global) {
	var Storage = function() {

	};

	Storage.prototype = {
		isSupported: function() {
			return ('localStorage' in global);
		},
		add: function(key, stringValue) {
			global.localStorage.setItem(key, stringValue);
		},
		addJSON: function(key, obj) {
			global.localStorage.setItem(key, JSON.stringify(obj));
		},
		get: function(key) {
			return global.localStorage.getItem(key);
		},
		getJSON: function(key) {
			var json = this.get(key);
			if (json !== null) {
				return JSON.parse(json);	
			}
			return null;
		},
		remove: function(key) {
			global.localStorage.removeItem(key);
		},
		clear: function() {
			global.localStorage.clear();
		}
	};

	return new Storage();
})(this);
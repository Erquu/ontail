/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var Tail = (function() {
	var sizesRegex = new RegExp('^bytes (\\d+)-(\\d+)\\/(\\d+)$', 'gm');

	var Tail = function(url, callback) {
		this.url = url;

		this._callback = callback;
		this._loading = false;
		this._logData = '';
		this._logSize = 0;
		this._loadSize = 30 * 1024; // 30KB
	};

	Tail.prototype = {
		reset: function() {
			this._logData = '';
			this._logSize = 0;
		},
		poll: function() {
			if (this._loading) {
				return;
			}
			this._loading = true;

			var range;
			if (this._logSize === 0) {
				range = '-' + this._loadSize.toString();
			} else {
				range = (this._logSize - 1).toString() + '-';
			}

			AjaxHelper.load({
				url: this.url,
				headers: {
					Range: 'bytes=' + range
				},
				cache: false,
				callback: this
			});
		},
		handleEvent: function(ev) {
			var req = ev.request;

			if (req.readyState === 4) {
				var data = req.response;

				var size;
				var added = false;

				this._loading = false;

				switch (req.status) {
					case 206:
						if (data.length > this._loadSize) {
							fireEvent(this._callback, {
								type: 'tailerror',
								request: req,
								reset: false,
								message: 'Expected 206 Partial Content'
							});
							this.reset();
							return;
						}

						var contentRange = req.getResponseHeader('Content-Range');
						if (!contentRange) {
							fireEvent(this._callback, {
								type: 'tailerror',
								request: req,
								reset: false,
								message: 'Server did not respond with a content range'
							});
							return;
						}

						sizesRegex.lastIndex = 0;
						var sizes = sizesRegex.exec(contentRange);

						if (!!sizes) {
							sizes = [
								parseInt(sizes[1]),
								parseInt(sizes[2]),
								parseInt(sizes[3])
							];

							size = sizes[2];
							if (isNaN(size)) {
								fireEvent(this._callback, {
									type: 'tailerror',
									request: req,
									reset: false,
									message: 'Invalid Content-Range size'
								});
								return;
							}
						} else {
							fireEvent(this._callback, {
								type: 'tailerror',
								request: req,
								reset: false,
								message: 'Invalid Content-Range size'
							});
							return;
						}
						break;
					case 416:
						fireEvent(this._callback, {
							type: 'tailerror',
							request: req,
							reset: true,
							message: 'File was truncated'
						});
						this.reset();
						return;
					case 200:
						if (this._logSize > 1) {
							fireEvent(this._callback, {
								type: 'tailerror',
								request: req,
								reset: false,
								message: 'Expected 206 Partial Content'
							});
							this.reset();
							return;
						}
						size = data.length;
						break;
				}

				if (this._logSize === 0) {
					if (data.length < size) {
						var start = data.indexOf('\n');
						this._logData = data.substring(start + 1);
					} else {
						this._logData = data;
					}

					added = true;
				} else {
					this._logData = data.substring(1);

					if (this._logData.length > this._loadSize) {
						var start = this._logData.indexOf('\n', this._logData.length - this._loadSize);
						this._logData = this._logData.substring(start + 1);
					}

					if (data.length > 1) {
						added = true;
					}
				}

				this._logSize = size;

				if (added) {
					fireEvent(this._callback, {
						type: 'tail',
						data: this._logData
					});
					// document.getElementById('content').innerHTML += this._logData;
					// scrollTo(0, document.body.scrollHeight);
				}
			}
		}
	};

	return Tail;
})();
/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

/**
 * ajaxhelper.js
 * Class for easy ajax setup
 * Supports both synchronous and asynchronous requests
 *
 * Example
 *   Async POST
 *     var request = (new AjaxHelper()).load({
 *       url: "ajax/test.php",
 *       data: "q=hello&order=desc",
 *       method: "POST",
 *       callback: function(ev) {alert(ev.type + ": " + ev.request.status);}
 *     });
 *  Sync GET
 *     var content = (new AjaxHelper()).load({
 *       url: "ajax/test.php",
 *       data: "q=hello&order=desc",
 *       sync: true
 *     });
 *
 * Requires listener.js
 */


if (typeof window.isset !== "function") {
	window.isset = function(obj) {
		return !(obj === undefined || obj === null);
	};
}

if (typeof window.fireEvent !== "function") {
	window.fireEvent = function(callback, args) {
		if (typeof callback === "function") {
			callback(args);
		} else if ((typeof callback === "object") && (typeof callback.handleEvent === "function")) {
			callback.handleEvent(args);
		}
	};
}

var AjaxHelper = (function() {
	var createRequest = function() {
		if (window.XMLHttpRequest)
			return new XMLHttpRequest();
		return new ActiveXObject("Microsoft.XMLHTTP");
	};

	var AjaxHelper = function() {
		//EventHandler.call(this);
	};

	AjaxHelper.prototype = {
		//__proto__:new EventHandler(),
		load: function(settings) {
			if (isset(settings) && isset(settings.url)) {
				settings.data = isset(settings.data) ? settings.data : null;

				settings.sync = isset(settings.sync) ? settings.sync : false;
				var async = !settings.sync;

				settings.method = isset(settings.method) ? settings.method.toUpperCase() : "GET";
				if (settings.method === "GET") {
					settings.url += ("?" + settings.data);
					settings.data = null;
				} else if (settings.method === "POST") {
					settings.headers = settings.headers || {};
					settings.headers["Content-Type"] = "application/x-www-form-urlencoded";
				}

				/** If no callback for async op is defined, fail */
				if (async && !isset(settings.callback)) {
					return false;
				}

				var request = createRequest();
				if (isset(settings.args)) {
					request.args = settings.args;
				}
				request.open(settings.method, settings.url, async);

				if (isset(settings.cache) && settings.cache === false) {
					settings.headers = settings.headers || {};

					settings.headers.Pragma = 'no-cache';
					settings.headers.Expires = '0';
					settings.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0';
					settings.headers['Last-Modified'] = new Date(0);
					settings.headers['If-Modified-Since'] = new Date(0);
				}

				if (isset(settings.headers)) {
					for (var key in settings.headers) {
						request.setRequestHeader(key, settings.headers[key]);
					}
				}

				/*
				if (isset(settings.contentType))
					request.setRequestHeader('Content-Type', settings.contentType);
				*/
				if (async) {
					request.callback = settings.callback;
					request.addEventListener("readystatechange", this);
				}

				request.send(settings.data);

				if (!async)
					return request.responseText;
				return request;
			}
		},
		handleEvent: function(ev) {
			ev.srcElement = ev.srcElement || ev.target;

			var request = ev.srcElement;

			var eventArgs = {
				type: "readystatechange",
				request: request
			};
			if (isset(request.args)) {
				eventArgs.args = request.args;
				delete request.args;	
			}
			fireEvent(ev.srcElement.callback, eventArgs);

			if (ev.srcElement.readyState === 4 && ev.srcElement.status === 200) {
				eventArgs.type = "ajaxready";
				fireEvent(ev.srcElement.callback, eventArgs);
			}
		}
	};

	return new AjaxHelper();
})();
/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var FileFetch = (function() {
	var FileFetch = function(url) {
		this.url = url;

		this._logFiles = [];
	};

	FileFetch.prototype = {
		fetch: function(callback) {
			this._callback = callback;
			AjaxHelper.load({
				url: this.url,
				cache: false,
				callback: this
			});
		},
		getLogFiles: function() {
			return this._logFiles;
		},
		handleEvent: function(ev) {
			if (ev.type === "ajaxready") {
				var data = ev.request.response;
				this._logFiles = [];

				var rx = new RegExp("<a href=\"([^\\/\\?][^\\\"]*.log)\">", "gmi");
				var t;
				while (!!(t = rx.exec(data))) {
					var log = t[1];
					this._logFiles.push(log);
				}

				fireEvent(this._callback, {
					type: "fetch",
					data: this._logFiles
				});
			}
		}
	};

	return FileFetch;

})();
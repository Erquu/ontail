/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

(function(global) {
  var WelcomePage = function(url, title, callback) {
    this._tabPage = null;
    this.domElement = null;
  };

  WelcomePage.prototype = {
    init: function(tabPage) {
      this._tabPage = tabPage;
      this._tabPage.title = 'Welcome';

      this.domElement = this._tabPage.getContentElement();
      //this.domElement.innerHTML = "";
      this.domElement.className = "welcome";

      AjaxHelper.load({
        url: "scripts/pages/info/welcome.html",
        callback: this
      });
    },
    handleEvent: function(ev) {
      switch (ev.type) {
        case "ajaxready":
          this.domElement.innerHTML = ev.request.responseText;

          this._selectLogLink = document.getElementById('welcome_selectlog');
          this._selectLogLink.addEventListener('click', this);

          this._openSettingsLink = document.getElementById('welcome_opensettings');
          this._openSettingsLink.addEventListener('click', this);
          break;
        case 'click':
          ev.srcElement = ev.srcElement || ev.target;

          if (isParent(ev.srcElement, this._selectLogLink)) {
            ui.createPage('logselector');
          } else if (isParent(ev.srcElement, this._openSettingsLink)) {
            ui.createPage('config');
          }
          break;
      }
    }
  };

  global.ui.registerPageOpenHandler('welcomepage', function(ui) {
    return new WelcomePage();
  });
})(this);
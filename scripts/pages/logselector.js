/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

(function(global) {
	var search = function(list, value) {
		if (list.indexOf(value) !== -1) {
			return value;
		} else {
			value = value.toLowerCase();
			for (var i = 0; i < list.length; i++) {
				var tmp = list[i].toLowerCase();
				if (tmp === value) {
					return list[i];
				}
			}
		}
		return undefined;
	};

	var LogSelector = function() {
		this._callback = ui;

		this._fileFetch = new FileFetch(ui._config.listURL);
		this._fileFetch.fetch(this);

		this._height = 0;
		this._visible = false;
	};

	LogSelector.prototype = {
		_filter: function() {
			var patternText = this._search.value;
			var hideOpenFiles = this._hideOpenFiles.checked;

			var pattern = null;
			var usePattern = (patternText.length > 0);
			if (usePattern) {
				pattern = new RegExp(patternText, "i");
			}

			var links = this.domElement.getElementsByTagName("a");
			for (var i = 0; i < links.length; i++) {
				var link = links[i];
				var value = link.getAttribute('value');
				var openend = (link.className.indexOf('open') !== -1);

				if ((usePattern && pattern.test(value)) || !usePattern) {
					link.style.display = openend && hideOpenFiles ? 'none' : 'block';
				} else {
					link.style.display = "none";
				}
			}
		},
		init: function(tabPage) {
			this._tabPage = tabPage;
			this._tabPage.title = 'Select Logfile';
			this._tabPage._autoScroll = false;

			this.domElement = this._tabPage.getContentElement();
			this.domElement.className = 'logselect';

			this.domElement.appendChild(document.createTextNode('Loading...'));
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case "fetch":
					this.domElement.innerHTML = '';

					this._search = document.createElement("input");
					this._search.type = 'text';
					this._search.placeholder = "Search logfile";
					this._search.addEventListener("keyup", this);
					this.domElement.appendChild(this._search);

					this._hideOpenFiles = document.createElement('input');
					this._hideOpenFiles.type = 'checkbox';
					this._hideOpenFiles.checked = 'checked';
					this._hideOpenFiles.addEventListener("change", this);
					this.domElement.appendChild(this._hideOpenFiles);
					this._hideLabel = document.createElement('label');
					this._hideLabel.appendChild(document.createTextNode('Hide open files'));
					this.domElement.appendChild(this._hideLabel);

					this.domElement.appendChild(document.createElement("br"));
					this.domElement.appendChild(document.createElement("br"));

					for (var i = 0; i < ev.data.length; i++) {
						var log = ev.data[i];

						var e = document.createElement("a");
						e.innerHTML = e.href = log;
						e.setAttribute("value", log);
						e.addEventListener("click", this);

						if (ui.hasPageWithTitle(log)) {
							e.className = 'open';
						}

						this.domElement.appendChild(e);
					}
					// break; // filter afterwards, so fallthrough
				case 'change':
				case "keyup":
					this._filter();
					break;
				case "click":
					ev.stopPropagation();
					ev.preventDefault();

					// Handles click on links for the logfiles (created in the "fetch" event)
					ev.srcElement = ev.srcElement || ev.target;

					var res = ev.srcElement.getAttribute("value");

					var createTabPage = true;
					if (ui.hasPageWithTitle(res)) {
						var pages = ui.getPagesByTitle(res);
						if (pages.length > 0) {
							ui.setActive(pages[0]);
							createTabPage = false;

							this._tabPage.selfDestroy();
						}
					}
					if (createTabPage) {
						var page = new TailPage(
							ui._config.logFolder + res,
							res);
						page.init(this._tabPage);
						this._tabPage.setContentPage(page);
					}
					break;
			}
		}
	};

	global.ui.registerPageOpenHandler('logselector', function(ui) {
		return new LogSelector();
	});
})(this);
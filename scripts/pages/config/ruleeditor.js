/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var RuleEditor = (function() {

	var cssPattern = /^\s*([\w-]+):\s*([\w\d#\(\)\-,"][\s\w\d#\(\)\-,]*[\w\d#\(\)\-,"])\s*;?\s*$/gmi;

	var RuleEditor = function() {
		this._loading = false;
		this._loaded = false;

		this._container = document.createElement("div");

		this._callback = undefined;

		this._pattern = "";
		this._wholeLine = false;
		this._blocking = false;
		this._styles = []; /*
			Notation will be
			{
				name: <CSS Property Name>,
				value: <CSS Property Value>
			}
		*/
	};

	RuleEditor.prototype = {
		setConfiguration: function(config) {
			this._pattern = config.pattern || "";
			this._wholeLine = config.line || false;
			this._blocking = config.blocking || false;
			this._styles = config.styles || [];
			this._applyChange();
		},
		setCallback: function(callback) {
			this._callback = callback;
		},
		getDomElement: function() {
			if (!this._loading) {
				AjaxHelper.load({
					url: "scripts/pages/config/configpage.html",
					callback: this
				});
				this._loading = true;
			}
			return this._container;
		},
		_renderCss: function() {
			var cssText = "element {\n";
			for (var i = 0; i < this._styles.length; i++) {
				cssText += "  " + this._styles[i].name + ": " + this._styles[i].value + ";\n";
			}
			cssText += "}";
			this._advanceStyleContainer.value = cssText;
		},
		_applyChange: function() {
			if (this._loaded) {
				this._patternInput.value = this._pattern;
				this._enableWholeLineCheck.checked = this._wholeLine;

				var style = this._getCss("color");
				if (style === null) {
					this._enableFontColorCheck.checked = false;
				} else {
					this._enableFontColorCheck.checked = true;
					this._fontColorChooser.value = style.value;
				}
				style = this._getCss("background-color");
				if (style === null) {
					this._enableBgColorCheck.checked = false;
				} else {
					this._enableBgColorCheck.checked = true;
					this._bgColorChooser.value = style.value;
				}

				this._enableBlock.disabled = this._wholeLine;
				this._enableBlock.checked = this._blocking;

				this._renderCss();

				fireEvent(this._callback, {
					type: "ruleedited",
					pattern: this._pattern,
					styles: this._styles,
					line: this._wholeLine,
					blocking: this._blocking
				});
			}
		},
		_getCss: function(name) {
			for (var i = 0; i < this._styles.length; i++) {
				if (this._styles[i].name === name) {
					return this._styles[i];
				}
			}
			return null;
		},
		_getCssValue: function(name) {
			var style = this._getCss(name);
			if (style !== null) {
				return style.value;
			}
			return null;
		},
		_removeCss: function(name) {
			for (var i = 0; i < this._styles.length; i++) {
				if (this._styles[i].name === name) {
					this._styles.splice(i, 1);
					break;
				}
			}
		},
		_addCssValue: function(name, value) {
			var style = this._getCss(name);
			if (style === null) {
				style = {};
				style.name = name;
				style.value = value;
				this._styles.push(style);
			} else {
				style.value = value;
			}
			return style;
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case "ajaxready":
					this._container.innerHTML = ev.request.responseText;

					this._patternInput = document.getElementById("pattern");
					this._patternInput.addEventListener("blur", this);

					this._enableWholeLineCheck = document.getElementById("markLine");
					this._enableWholeLineCheck.addEventListener("change", this);

					this._enableFontColorCheck = document.getElementById("enableFontColor");
					this._enableFontColorCheck.addEventListener("change", this);

					this._fontColorChooser = document.getElementById("fontColor");
					this._fontColorChooser.addEventListener("change", this);

					this._enableBgColorCheck = document.getElementById("enableBgColor");
					this._enableBgColorCheck.addEventListener("change", this);

					this._bgColorChooser = document.getElementById("bgColor");
					this._bgColorChooser.addEventListener("change", this);

					this._enableBlock = document.getElementById("blocking");
					this._enableBlock.addEventListener("change", this);

					this._advanceStyleContainer = document.getElementById("cssStyle");
					this._advanceStyleContainer.addEventListener("blur", this);

					this._moveUpButton = document.getElementById("moveUp");
					this._moveUpButton.addEventListener("click", this);

					this._moveDownButton = document.getElementById("moveDown");
					this._moveDownButton.addEventListener("click", this);

					this._deleteButton = document.getElementById("delete");
					this._deleteButton.addEventListener("click", this);

					this._loaded = true;
					break;
				case "change":
					ev.srcElement = ev.srcElement || ev.target;
					this.handleChange(ev);
					break;
				case "blur":
					this._pattern = this._patternInput.value;
					
					cssPattern.lastIndex = 0;
					var text = this._advanceStyleContainer.value;
					var result;
					this._styles = []; // Reset styles
					while ((result = cssPattern.exec(text)) !== null) {
						this._addCssValue(result[1], result[2]);
					}
					break;
				case "click":
					ev.srcElement = ev.srcElement || ev.target;
					if (ev.srcElement === this._deleteButton) {
						if (confirm("Are you sure you want to delete this rule?")) {
							fireEvent(this._callback, {
								type: "ruledeleted"
							});
						}
						return;
					} else {
						// Move button clicked
						var dir = (ev.srcElement === this._moveUpButton ? 'up' : 'down');
						fireEvent(this._callback, {
							type: "rulemove",
							direction: dir,
							up: (dir === 'up'),
							down: (dir === 'down')
						});
					}
					break;
			}
			this._applyChange();
		},
		handleChange: function(ev) {
			switch (ev.srcElement) {
				case this._patternInput:
					this._pattern = this._patternInput.value;
					break;
				case this._enableBgColorCheck:
					if (this._enableBgColorCheck.checked) {
						var preValue = this._getCssValue("background-color");
						if (preValue === null) {
							this._addCssValue("background-color", this._bgColorChooser.value);
						} else {
							this._bgColorChooser.value = preValue;
						}
					} else {
						this._removeCss("background-color");
					}
					break;
				case this._bgColorChooser:
					this._enableBgColorCheck.checked = true;
					this._addCssValue("background-color", this._bgColorChooser.value);
					break;
				case this._enableBlock:
					this._blocking = this._enableBlock.checked;
					break;
				case this._enableFontColorCheck:
					if (this._enableFontColorCheck.checked) {
						var preValue = this._getCssValue("color");
						if (preValue === null) {
							this._addCssValue("color", this._fontColorChooser.value);
						} else {
							this._fontColorChooser.value = preValue;
						}
					} else {
						this._removeCss("color");
					}
					break;
				case this._fontColorChooser:
					this._enableFontColorCheck.checked = true;
					this._addCssValue("color", this._fontColorChooser.value);
					break;
				case this._enableWholeLineCheck:
					this._wholeLine = this._enableWholeLineCheck.checked;
					this._enableBlock.disabled = this._wholeLine;
					break;
			}
		}
	};

	return RuleEditor;
})();
/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

var RuleLine = (function() {
	var testSentence = "The quick brown fox jumps over the lazy dog";
	var testPattern = "quick brown";

	var hasChild = function(container, e) {
		for (var child = container.firstChild; !!child; child = child.nextSibling) {
			if (child === e) {
				return true;
			}
		}
		return false;
	};

	var RuleLine = function(container, editor, callback, rule) {
		this._rule = rule || {
			id: guid(),
			pattern: "",
			line: false,
			blocking: false,
			styles: []
		};

		this._callback = callback;

		this._renderer = new Renderer(testPattern);
		this._editor = editor;

		this._lineContainer = container;
		this._lineContainer.className = "rule";
		this._lineContainer.addEventListener("click", this);

		this._testContainer = document.createElement("div");
		this._lineContainer.appendChild(this._testContainer);

		this._previewRule();
	};

	RuleLine.prototype = {
		_previewRule: function() {
			this._renderer._styles = this._rule.styles;
			this._renderer._wholeLine = this._rule.line;
			this._renderer._blocking = this._rule.blocking;
			this._renderer._renderStyleString();
			this._testContainer.innerHTML = this._renderer.apply(testSentence);
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case "click":
					//ev.srcElement = ev.srcElement || ev.target;
					if (this._lineContainer.children.length === 1) {
						this._lineContainer.appendChild(this._editor.getDomElement());
						this._editor.setCallback(this);
						this._editor.setConfiguration(this._rule);
					}
					break;
				case "rulemove":
					ev.rule = this;
					fireEvent(this._callback, ev);
					break;
				case "ruleedited":
					this._rule.pattern = ev.pattern;
					this._rule.line = ev.line;
					this._rule.styles = ev.styles;
					this._rule.blocking = ev.blocking;

					this._previewRule();

					if (Storage.isSupported()) {
						var ruleList = Storage.getJSON("rules");
						if (ruleList === null) {
							ruleList = [];
						}

						var added = false;
						for (var i = 0; i < ruleList.length; i++) {
							if (ruleList[i].id === this._rule.id) {
								ruleList[i] = this._rule;
								added = true;
							}
						}
						if (!added) {
							ruleList.push(this._rule);
						}

						Storage.addJSON("rules", ruleList);
						console.log("Rule saved - uuid: " + this._rule.id);
						fireEvent(this._callback, {
							type: "rulesaved",
							rule: this
						});
					}
					break;
				case "ruledeleted":
					if (Storage.isSupported()) {
						var ruleList = Storage.getJSON("rules");
						
						if (ruleList !== null) {
							for (var i = 0; i < ruleList.length; i++) {
								if (ruleList[i].id === this._rule.id) {
									ruleList.splice(i, 1);
								}
							}

							Storage.addJSON("rules", ruleList);
							console.log("Rule deleted - uuid: " + this._rule.id);
						}
					}
					fireEvent(this._callback, {
						type: ev.type,
						rule: this
					});
					break;
			}
		}
	};

	return RuleLine;
})();
/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

(function(global) {

	var globalFilter = [];
	var globalGrep = [];

	/**
	 * @return {boolean} Returns false if the given line should not be shown
	 */
	var testFilter = function(line) {
		for (var i = 0; i < globalFilter.length; i++) {
			if (globalFilter[i].test(line)) {
				return true;
			}
		}
		return false;
	};

	/**
	 * @return {boolean} Returns true if the given line should be shown
	 */
	var testGrep = function(line) {
		for (var i = 0; i < globalGrep.length; i++) {
			if (globalGrep[i].test(line)) {
				return true;
			}
		}
		return (globalGrep.length === 0);
	};

	var TailPage = function(url, title) {
		this.active = false;

		this._maxCharacters = (64 * 1024 * 1024); // 0 = infinite

		this._url = url;
		this._title = title;

		this._loaded = false;
		this._paused = false;
		this._autoScroll = true;

		this._split = false;

		this._uuid = null;

		this._tabPage = null;
		this._tailInstance = null;
		this.domElement = null;
	};

	TailPage.prototype = {
		_renderContent: function(data) {
			var output = '';

			var meta = {};
			var line = '';
			var empty = true;
			var lineBreak = !this._split;

			var tmp = data.split('\n');
			for (var i = 0; i < tmp.length; i++) {
				line = tmp[i];
				if (testGrep(line) && !testFilter(line)) {
					output += ui.renderLine(htmlspecialchars(line), meta);
					if (i === (tmp.length - 1)) {
						continue;
					}
					// TODO: FIX renderer is now an array
					if (lineBreak && (!meta.success  || !meta.wholeLine)) {
						output += '<br />';
					}
					meta = {}; // Reset

					lineBreak = true;
					empty = false;
				}
			}
			if (!empty) {
				output += '<hr />';
				this._split = true;
			} else {
				this._split = false;
			}

			return output;
		},
		init: function(tabPage) {
			this._tabPage = tabPage;
			this._tabPage.title = this._title;

			this.domElement = this._tabPage.getContentElement();
			this.domElement.innerHTML = '';

			this._tailInstance = new Tail(this._url, this);
		},
		resize: function(width, height) {
			this.domElement.style.height = height + 'px';
			this.domElement.style.overflow = 'auto';
		},
		activate: function() {
			if (Storage.isSupported() && this._uuid !== null) {
				var content = Storage.get(this._uuid);
				if (content !== null) {
					this.domElement.innerHTML = content;
					console.log('Content deserialized from local storage');
				}
			}
		},
		deactivate: function() {
			if (Storage.isSupported()) {
				if ('crypto' in window && this._uuid === null) {
					this._uuid = guid();
				}

				if (this._uuid !== null) {
					var contentSaved = true;

					try {
						Storage.add(this._uuid, this.domElement.innerHTML);
						console.log('Content serialized into local storage');
					} catch (e) {
						contentSaved = false;
					}

					if (contentSaved) {
						this.domElement.innerHTML = '';
					}
				}
			}
		},
		dispose: function() {
			if (Storage.isSupported() && this._uuid !== null) {
				Storage.remove(this._uuid);
			}
		},
		poll: function() {
			if (!this._paused) {
				this._tailInstance.poll();
			}
		},
		scrollToBottom: function() {
			var elem = this.domElement.parentNode;
			elem.scrollTop = (elem.scrollHeight - elem.offsetHeight);
		},
		handleEvent: function(ev) {
			this._autoScroll = (this.domElement.scrollTop >= (this.domElement.scrollHeight - this.domElement.offsetHeight));
			switch (ev.type) {
				case 'tailerror':
					if (ev.reset === true) {
						this.domElement.innerHTML = '';
						this.domElement.appendChild(document.createTextNode('Resetting page because of error:'));	
						this.domElement.appendChild(document.createElement('br'));
					}

					var errorNode = document.createElement('span');
					errorNode.className = 'tailerror';
					errorNode.appendChild(document.createTextNode('*** ' + ev.message + ' ***'));
					this.domElement.appendChild(errorNode);
					this.domElement.appendChild(document.createElement('br'));
					break;
				case 'tail':
					if (!this._tabPage._active) {
						this._tabPage.getTabElement().className = 'update';
					}

					this.domElement.innerHTML += this._renderContent(ev.data);

					if (this._maxCharacters > 0) {
						var totalLength = this.domElement.innerHTML.length;
						var cutLength = -this._maxCharacters;

						this.domElement.innerHTML = this.domElement.innerHTML.substr(cutLength);
					}
					break;
			}
			if (this._autoScroll) {
				this.scrollToBottom();
			}
		}
	};

	TailPage.addGlobalFilter = function(pattern, mod) {
		globalFilter.push(new RegExp(pattern, mod));
	};

	TailPage.addGrep = function(pattern, mod) {
		globalGrep.push(new RegExp(pattern, mod));
	};

	global.TailPage = TailPage;
})(this);
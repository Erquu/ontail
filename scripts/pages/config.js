/*
    Copyright (c) 2012-2015 Patrick Schmidt (http://megaschmidt.com)

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
*/

(function(global) {
	var hideCallback = function(element) {
		return function() {
			element.className = "fade";
		};
	};

	var ConfigPage = function() {
		this._timoutID = -1;
		this._lines = [];

		this._editor = null;
		this._page = null;

		this._addButton = null;
		this._hintContainer = null;

		this.domElement = null;
	};

	ConfigPage.prototype = {
		init: function(tabPage) {
			this._page = tabPage;
			this._page.title = "Configuration";
			this._page._autoScroll = false;

			this._editor = new RuleEditor();

			this.domElement = this._page.getContentElement();
			this.domElement.className = "config";
			
			this.domElement.appendChild(document.createElement("br")); // TODO: Add styling for this page
			this._addButton = document.createElement("input");
			this._addButton.type = "button";
			this._addButton.value = "Add Rule";
			this._addButton.addEventListener("click", this);
			this.domElement.appendChild(this._addButton);

			this._hintContainer = document.createElement("span");
			this._hintContainer.innerHTML = "Click on a line to edit it";
			this.domElement.appendChild(this._hintContainer);

			this.domElement.appendChild(document.createElement("br"));
			this.domElement.appendChild(document.createElement("br"));

			if (!Storage.isSupported()) {
				this._showMessage("red", "LocalStorage not supported by your browser, rules will be deleted after you leave this site!");
			} else {
				var rules = Storage.getJSON("rules");
				if (rules !== null) {
					for (var i = 0; i < rules.length; i++) {
						var rule = rules[i];
						var container = document.createElement("div");
						this.domElement.appendChild(container);
						var line = new RuleLine(container, this._editor, this, rule);
						this._lines.push(line);
					}
				}
			}
		},
		_showMessage: function(color, msg) {
			this._hintContainer.className = "";
			this._hintContainer.style.display = "inline-block";
			this._hintContainer.style.color = color;
			this._hintContainer.innerHTML = msg;

			clearTimeout(this._timoutID);

			this._timoutID = setTimeout(hideCallback(this._hintContainer), 2000);
		},
		_persistRules: function() {
			var tmpRules = [];
			for (var i = 0; i < this._lines.length; i++) {
				tmpRules.push(this._lines[i]._rule);
			}
			Storage.addJSON("rules", tmpRules);
			console.log("Rules updated");
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case "click":
					var container = document.createElement("div");
					this.domElement.appendChild(container);
					var line = new RuleLine(container, this._editor, this);
					this._lines.push(line);
					break;
				case "rulemove":
					var index = this._lines.indexOf(ev.rule);
					if (index !== -1) {
						if (ev.up) {
							if (index > 0) {
								var prevRule = this._lines[index-1];
								var rule = this._lines.splice(index, 1)[0];
								this._lines.splice(index - 1, 0, rule);

								this.domElement.removeChild(rule._lineContainer);
								this.domElement.insertBefore(rule._lineContainer, prevRule._lineContainer)
							}
						} else {
							if (index === this._lines.length - 2) {
								// Rule will be at the end after modification
								var rule = this._lines.splice(index, 1)[0];
								this._lines.push(rule);

								this.domElement.removeChild(rule._lineContainer);
								this.domElement.appendChild(rule._lineContainer);
							} else if (index < (this._lines.length - 2)) {
								var prevRule = this._lines[index + 1];
								var rule = this._lines.splice(index, 1)[0];
								this._lines.splice(index + 1, 0, rule);

								this.domElement.removeChild(rule._lineContainer);
								this.domElement.insertBefore(rule._lineContainer, prevRule._lineContainer.nextSibling)
							}
						}
						this._persistRules();
					}
					break;
				case "rulesaved":
					this._showMessage("#006600", "Rule saved");
					ui.resetRenderer();
					break;
				case "ruledeleted":
					this.domElement.removeChild(ev.rule._lineContainer);
					this._showMessage("#006600", "Rule deleted");
					ui.resetRenderer();
					break;
			}
		}
	};

	global.ui.registerPageOpenHandler('config', function(ui) {
		return new ConfigPage();
	});
})(this);
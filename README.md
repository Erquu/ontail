# on-tail
OnTail is a web application to tail files.  
You can also create highlighting rules to bring color to your files.

Installation
------------
* Clone the repo into your www folder.
* Edit the config.json file in the ontail root folder to fit your needs

Configuration
-------------
Following settings are available:

**title**  
The page-title will be replaced by this title if it is set.  
**listURL**  
Path to the file used for the file listing.  
**logFolder**  
Path to the folder containing the log files. The filenames from the listing page will be concatenated with this path.
